# ~/.bashrc: executed by bash(1) for non-login shells.

alias dir='dir --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias grep='grep --color=auto'
alias l='ls -CF'
alias la='ls -A'
alias ll='ls -l'
alias lh='ls -lh'
alias l1='ls -1'
alias ls='ls --color=auto'
alias vdir='vdir --color=auto'
alias vi='vim'
alias summary='grep --color=no ^[^#]'

export HISTCONTROL=ignoreboth
export LC_ALL=$LANG

set_prompt_color()
{

        txtblk='\e[0;30m' # Black - Regular
        txtred='\e[0;31m' # Red
        txtgrn='\e[0;32m' # Green
        txtylw='\e[0;33m' # Yellow
        txtblu='\e[0;34m' # Blue
        txtpur='\e[0;35m' # Purple
        txtcyn='\e[0;36m' # Cyan
        txtwht='\e[0;37m' # White
        bldblk='\e[1;30m' # Black - Bold
        bldred='\e[1;31m' # Red
        bldgrn='\e[1;32m' # Green
        bldylw='\e[1;33m' # Yellow
        bldblu='\e[1;34m' # Blue
        bldpur='\e[1;35m' # Purple
        bldcyn='\e[1;36m' # Cyan
        bldwht='\e[1;37m' # White
        unkblk='\e[4;30m' # Black - Underline
        undred='\e[4;31m' # Red
        undgrn='\e[4;32m' # Green
        undylw='\e[4;33m' # Yellow
        undblu='\e[4;34m' # Blue
        undpur='\e[4;35m' # Purple
        undcyn='\e[4;36m' # Cyan
        undwht='\e[4;37m' # White
        bakblk='\e[40m'   # Black - Background
        bakred='\e[41m'   # Red
        badgrn='\e[42m'   # Green
        bakylw='\e[43m'   # Yellow
        bakblu='\e[44m'   # Blue
        bakpur='\e[45m'   # Purple
        bakcyn='\e[46m'   # Cyan
        bakwht='\e[47m'   # White
        txtrst='\e[0m'    # Text Reset

        WHO=`whoami`
        if [ x"$WHO" = x"root" ] ; then
                export PS1="\[\e[01;33m\]\u@\h\[\e[0m\]\[\e[00;37m\] \[\e[0m\]\[\e[01;34m\]\w\[\e[0m\]\[\e[00;37m\] \\$ \[\e[0m\]"
        else
                export PS1="\[\e[00;33m\]\u@\h\[\e[0m\]\[\e[00;37m\] \[\e[0m\]\[\e[01;34m\]\w\[\e[0m\]\[\e[00;37m\] \\$ \[\e[0m\]"
        fi

}

set_prompt_color

[ -f /etc/bash_completion ] && . /etc/bash_completion


#!/usr/bin/perl
#
use strict;
use DBI;

my $dbh =  DBI-> connect('dbi:ODBC:asteraidconfigserver') or die "$DBI::err\n$DBI::errstr\n$DBI::state\n";
my $sql = qq/select filename, category, cat_metric, var_name, var_metric, var_val, commented from vConfig where filename not in ('sip.conf', 'extensions.conf')
union all    select filename, category, cat_metric, var_name, var_metric, var_val, commented from vSipConf
union all    select filename, category, cat_metric, var_name, var_metric, var_val, commented from vExtensionsConf
             order by filename, category, cat_metric, var_metric
/;

my $sth = $dbh->prepare($sql); $sth->{'LongReadLen'} = 20000; $sth->execute();
my $f = ''; my $c = '';

while (my $row = $sth->fetchrow_hashref) {
    if ($row->{filename} ne $f) {
        close C;
        $f = $row->{filename};
        $c = '';
        open C, ">:utf8", $f or die "Could not open file for write results: $!";
    };
    if ( $row->{category} ne $c ) {
        $c =  $row->{category};
        printf C "\n[%s]\n", $c;
    }
    if ( $f eq 'extensions.conf' ) {
        printf C "%5s = %s\n", $row->{var_name}, $row->{var_val};
    } else {
        printf C "%-15s = %s\n", $row->{var_name}, $row->{var_val};
   }
};

close C;

$dbh->disconnect if ($dbh);

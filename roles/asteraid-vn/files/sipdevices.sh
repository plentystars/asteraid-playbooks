#!/bin/sh

for EXT in $(asterisk -rx 'database show SIP Registry' | awk -F':' '/\/SIP\/Registry\// { gsub(/\/SIP\/Registry\/| /, ""); printf "%s:%s\n", $1, $2 }'); do
  UA=$(asterisk -rx "sip show peer ${EXT%%:*}" | awk -F' : ' '/Useragent/ { print $2}' ) 
  echo ${EXT}:${UA}
done

CREATE TABLE IF NOT EXISTS `syslog` (
  `dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `host` char(64) DEFAULT 'localhost',
  `tag` char(80) DEFAULT NULL,
  `status` char(80) DEFAULT NULL,
  `msg` text,
  `version` bigint unsigned DEFAULT 0
)  DEFAULT CHARSET=utf8;

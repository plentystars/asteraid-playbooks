#!/bin/bash

[ -r /opt/plentystars/asteraid/config ] && . /opt/plentystars/asteraid/config
[ -r /opt/plentystars/asteraid/config_user ] && . /opt/plentystars/asteraid/config_user

playbookdir=/opt/plentystars/asteraid-playbooks
serial=${2-`date +%s`}

dump(){

  [ ! -d ${storeDir}/${dumpDir} ] && mkdir -p ${storeDir}/${dumpDir}

  db_version=$(mysql -u${db_user} -p${db_password} -NBe "select var_val from user_settings where category = 'database' and var_name = 'version' " ${db_database})

  DUMPNAME=${storeDir}/${dumpDir}/conf-$(date +%Y%m%d-%H%M%S)_${db_version}.sql

  echo "-- db_version=${db_version}
  --
  -- " > $DUMPNAME

  # mysqldump filters against bug http://bugs.mysql.com/bug.php?id=24680
  mysqldump --routines ${db_database} | \
  sed -e 's/DEFINER=`\S*`\@`\S*` //g' -e 's/SQL SECURITY DEFINER/SQL SECURITY INVOKER/g' -e 's/),(/),\n(/g' >> $DUMPNAME

  gzip $DUMPNAME
}

send(){
 cd ${playbookdir}
 ansible-playbook ${playbookdir}/send.yml
}

reload(){
  cd ${playbookdir}
  ansible-playbook ${playbookdir}/reload.yml
}

restart(){
  cd ${playbookdir}
  ansible-playbook ${playbookdir}/restart.yml
}

case $1 in
  send)
    dump
    send $2
  ;;
  reload)
    reload
  ;;
  restart)
    restart
  ;;

esac

#!/usr/bin/python 
import datetime 
def main():
        module = AnsibleModule(
                argument_spec = dict(
                   command   = dict(default='core show uptime')
                )
        )

        (rc, out, stderr) = module.run_command("asterisk -rx '" + module.params['command'] + "'")


        module.exit_json(changed=False, ouput=out.split('\n'))


from ansible.module_utils.basic import *

if __name__ == '__main__':
  main()
#date = str(datetime.datetime.now()) 
#print json.dumps({ "time" : date })
